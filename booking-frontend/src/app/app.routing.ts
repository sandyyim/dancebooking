import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { CourseDetailsComponent } from './booking/course-details/course-details.component';
import { BookingComponent } from './booking/booking.component';
import { CourseBookingComponent } from './booking/course-booking/course-booking.component';
import { BookingHistoryComponent } from './booking/booking-history/booking-history.component';
import { CourseSearchComponent } from './booking/course-search/course-search.component';
import { PackageComponent } from './booking/package/package.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SignUpComponent } from './sign-up/sign-up.component';

export class AppRouting {}

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'logout', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'member', component: MemberDetailsComponent },
  { path: 'course/browse', component: CourseDetailsComponent },
  { path: 'course/book/preview', component: CourseBookingComponent },
  { path: 'member/booking-histories', component: BookingHistoryComponent },
  { path: 'course/search', component: CourseSearchComponent },
  { path: 'member/package', component: PackageComponent },
  { path: 'changepassword', component: ChangePasswordComponent },
  { path: '**', redirectTo: '' },
];

export const routing = RouterModule.forRoot(appRoutes);
