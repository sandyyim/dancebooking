export class Course {
  courseId: string;
  courseDate: string;
  courseStartTime: string;
  courseEndTime: string;
  tutorName: string;
  courseName: string;
  numberOfPoints: string;
  capacity: Number;
  isCourseAvailable: Boolean;
}
