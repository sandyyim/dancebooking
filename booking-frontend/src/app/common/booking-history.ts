export class BookingHistory {
  bookingId: string;

  courseId: string;

  courseName: string;

  memberName: string;

  courseDate: string;

  courseStartTime: string;

  numberOfPoints: number;

  cancelled: boolean;

  memberId: string;

  click: Boolean = true;
}
