export class PasswordDto {
  username: string;
  oldPassword: string;
  newPassword: string;
}
