export class PackageDetails {
  packageId: string;

  packageType: string;

  packageFromDate: string;

  packageToDate: string;

  packageBalance: number;

  memberName: string;
}
