import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PasswordDto } from '../common/password-dto';
import { ConfirmPasswordValidator } from '../confirm-password-validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
})
export class ChangePasswordComponent implements OnInit {
  username: string;
  oldPassword: string;
  newPassword: string;
  result: any;
  model: any = {};

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    this.username = sessionStorage.getItem('username');
    this.password();
  }

  changePassword() {
    let url = 'http://localhost:8080/changepassword';
    return this.httpClient
      .post<any>(url, {
        username: this.username,
        oldPassword: this.model.oldPassword,
        newPassword: this.model.newPassword,
      })
      .subscribe((data) => {
        this.result = data;
        if (this.result == 'Failed') {
          alert('Change Password Failed! ');
        } else {
          alert('Change Password Success! ');
        }
      });
  }

  password() {
    const { value: newPassword } = this.model.newPassword;
    const { value: confirmNewPassword } = this.model.confirmNewPassword;
    return newPassword === confirmNewPassword
      ? null
      : { passwordNotMatch: true };
  }
}
