import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app.routing';
import { LogoutComponent } from './logout/logout.component';
import { authInterceptorProviders } from './basic-auth-http-interceptor.service';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { CourseDetailsComponent } from './booking/course-details/course-details.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarHeaderComponent } from './calendar-header/calendar-header.component';
import { CommonHeaderComponent } from './common-header/common-header.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BookingComponent } from './booking/booking.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CourseBookingComponent } from './booking/course-booking/course-booking.component';
import { BookingService } from './booking/booking.service';
import { JwPaginationModule } from 'jw-angular-pagination';
import { BookingHistoryComponent } from './booking/booking-history/booking-history.component';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
} from '@angular/material/paginator';
import { CustomPaginator } from './custom-paginator-configuration';
import { CourseSearchComponent } from './booking/course-search/course-search.component';
import { FilterPipe } from './filter-pipe';
import { PackageComponent } from './booking/package/package.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'member', component: MemberDetailsComponent },
  { path: 'course/browse', component: CourseDetailsComponent },
  { path: 'member/booking-histories', component: BookingHistoryComponent },
  { path: 'course/book', component: CourseBookingComponent },
  { path: 'course/search', component: CourseSearchComponent },
  { path: 'member/package', component: PackageComponent },
  { path: 'changepassword', component: ChangePasswordComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignUpComponent,
    LogoutComponent,
    MemberDetailsComponent,
    CourseDetailsComponent,
    CalendarHeaderComponent,
    CommonHeaderComponent,
    CourseBookingComponent,
    BookingHistoryComponent,
    CourseSearchComponent,
    FilterPipe,
    PackageComponent,
    ChangePasswordComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    MaterialModule,
    BrowserAnimationsModule,
    NgbModule,
    JwPaginationModule,
    MatPaginatorModule,
  ],

  providers: [authInterceptorProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
