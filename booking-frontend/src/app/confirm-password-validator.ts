import { Attribute } from '@angular/compiler';
import { Directive, forwardRef } from '@angular/core';
import {
  AbstractControl,
  FormGroup,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

// @Directive({
//   selector: ‘[validateEqual][ngModel]’,
//   providers: [
//     provide(NG_VALIDATORS, { useExisting: forwardRef(() =>
//       ConfirmPasswordValidator), multi: true })
//   ]
// })
export class ConfirmPasswordValidator implements Validator {
  validateEqual: boolean;

  // constructor( @Attribute(‘validateEqual’) public validateEqual: string) {}
  validate(control: AbstractControl): ValidationErrors {
    // self value (e.g. retype password)
    let v = control.value; // control value (e.g. password)
    let e = control.root; // value not equal
    if (e && v !== e.value)
      return {
        validateEqual: false,
      };
    return null;
  }
  registerOnValidatorChange?(fn: () => void): void {
    throw new Error('Method not implemented.');
  }
}
