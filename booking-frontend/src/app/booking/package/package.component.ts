import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatPaginatorIntl, PageEvent } from '@angular/material/paginator';
import { map } from 'rxjs/operators';
import { PackageDetails } from 'src/app/common/package-details';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css'],
})
export class PackageComponent extends MatPaginatorIntl implements OnInit {
  memberName: string;
  datasource: PackageDetails[];
  resultsLength: number;
  totalElements: number;

  viewPage: number;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;

  constructor(private httpClient: HttpClient) {
    super();
  }

  ngOnInit(): void {
    this.memberName = sessionStorage.getItem('username');
    this.loadPackageHistories(this.memberName, '0').subscribe((result) => {
      this.datasource = result.content;
      this.resultsLength = result.totalElements;
    });
  }

  onPageFired(event) {
    this.loadPackageHistories(this.memberName, event.pageIndex).subscribe(
      (result) => {
        this.datasource = result.content;
      }
    );
  }

  loadPackageHistories(memberName: string, page: string) {
    let url = 'http://localhost:8080/member/package';

    let options = {
      params: new HttpParams()
        .append('page', page)
        .append('size', '10')
        .append('member_name', memberName),
    };

    return this.httpClient
      .post<GetResponse>(url, {}, options)
      .pipe(map((data) => data.packageDetails));
  }
}

interface GetResponse {
  packageDetails: {
    content: PackageDetails[];
    totalElements: number;
  };
}
