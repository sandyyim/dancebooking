import { Component, OnInit, ViewChild } from '@angular/core';
import { BookingHistory } from 'src/app/common/booking-history';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PageEvent } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { CustomPaginator } from 'src/app/custom-paginator-configuration';
import format from 'date-fns/format';

@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.component.html',
  styleUrls: ['./booking-history.component.css'],
  providers: [{ provide: MatPaginatorIntl, useValue: CustomPaginator() }],
})
export class BookingHistoryComponent
  extends MatPaginatorIntl
  implements OnInit {
  items: any = [];
  pageOfItems: Array<any>;
  page: any;
  size: any;
  datasource: BookingHistory[];

  totalElements: number;

  viewPage: number;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  resultsLength: number;

  cancelResult: BookingHistory;

  memberName: string;

  currentDate: Date = new Date();

  constructor(private httpClient: HttpClient) {
    super();
  }

  ngOnInit() {
    this.memberName = sessionStorage.getItem('username');
    this.loadBookingHistories(this.memberName, '0').subscribe((result) => {
      this.datasource = result.content;
      this.resultsLength = result.totalElements;
      this.checkClassDate(this.datasource);
    });
  }

  ngDoCheck() {
    this.checkClassDate(this.datasource);
  }

  onPageFired(event) {
    this.loadBookingHistories(this.memberName, event.pageIndex).subscribe(
      (result) => {
        this.datasource = result.content;
      }
    );
  }

  loadBookingHistories(memberName: string, page: string) {
    let url =
      'http://localhost:8080/member/' + memberName + '/booking-histories';

    let options = {
      params: new HttpParams().append('page', page).append('size', '10'),
    };

    return this.httpClient
      .post<GetResponse>(url, {}, options)
      .pipe(map((data) => data.bookingHistories));
  }

  cancelBooking(bookingId: string) {
    // this.memberName = sessionStorage.getItem('username');

    let url = 'http://localhost:8080/member/booking/cancel';

    let options = {
      params: new HttpParams()
        .append('member_name', this.memberName)
        .append('booking_id', bookingId),
    };

    return this.httpClient.post<any>(url, {}, options).subscribe((response) => {
      this.cancelResult = response;
      if (this.cancelResult.click == true) {
        alert('Cancellation is Failed! ');
      } else {
        this.cancelResult.click == true;
        alert('Cancellation is Success! ');
      }
    });
  }

  checkClassDate(bookingHistories: BookingHistory[]): void {
    let currentDateInString = format(this.currentDate, 'yyyy-MM-dd');
    for (var bookingHistory of bookingHistories) {
      if (
        bookingHistory.courseDate < currentDateInString ||
        bookingHistory.cancelled
      ) {
        bookingHistory.click = true;
      }
    }
  }
}

interface GetResponse {
  bookingHistories: {
    content: BookingHistory[];
    totalElements: number;
  };
}
