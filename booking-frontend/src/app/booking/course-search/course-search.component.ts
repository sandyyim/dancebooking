import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { format } from 'date-fns';
import { map } from 'rxjs/operators';
import { Course } from 'src/app/common/course';
import { BookingService } from '../booking.service';

@Component({
  selector: 'app-course-search',
  templateUrl: './course-search.component.html',
  styleUrls: ['./course-search.component.css'],
})
export class CourseSearchComponent implements OnInit {
  datasource: Course[];
  resultsLength: number;
  model: any = {};
  loading: boolean = false;

  currentDate: Date = new Date();
  nextDay: Date = new Date();

  // isCourseAvailable = false;

  constructor(
    private httpClient: HttpClient,
    private bookingService: BookingService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.showCourseDetails('0').subscribe((result) => {
      this.datasource = result.content;
      this.resultsLength = result.totalElements;
      for (let i of this.datasource) {
        this.handleBooking(i);
      }
    });
  }

  onPageFired(event) {
    this.showCourseDetails(event.pageIndex).subscribe((result) => {
      this.datasource = result.content;
      for (let i of this.datasource) {
        this.handleBooking(i);
      }
    });
  }

  buttonClicked(page: string) {
    this.loading = true;
    this.showCourseDetails(page).subscribe((result) => {
      this.datasource = result.content;
      for (let i of this.datasource) {
        this.handleBooking(i);
      }
    });
    if (this.datasource) {
      this.loading = false;
    }
  }

  showCourseDetails(
    page?: string,
    courseName?: string,
    tutorName?: string,
    fromDate?: string,
    toDate?: string
  ) {
    let url = 'http://localhost:8080/course/search';

    let params = new HttpParams();

    if (this.model.courseName != null) {
      params = params.set('courseName', this.model.courseName);
    }
    if (this.model.tutorName != null) {
      params = params.set('tutorName', this.model.tutorName);
    }
    if (this.model.fromDate != null) {
      params = params.set('fromDate', this.model.fromDate);
    }
    if (this.model.toDate != null) {
      params = params.set('toDate', this.model.toDate);
    }

    params = params.set('page', page);
    let options = {
      params,
    };

    return this.httpClient
      .post<GetResponse>(url, {}, options)
      .pipe(map((data) => data.courseDetails));
  }

  searchCourseDetails(
    courseName: string,
    tutorName: string,
    fromDate: string,
    toDate: string,
    page: string
  ) {
    let url = 'http://localhost:8080/course/search';

    let options = {
      params: new HttpParams()
        .append('courseName', courseName)
        .append('tutorName', tutorName)
        .append('fromDate', fromDate)
        .append('toDate', toDate)
        .append('page', page)
        .append('size', '10'),
    };

    return this.httpClient
      .post<GetResponse>(url, {}, options)
      .pipe(map((data) => data.courseDetails));
  }

  handleBooking(course: Course) {
    let currentDateInString = format(this.currentDate, 'yyyy-MM-dd');
    this.nextDay.setDate(this.currentDate.getDate() + 1);
    let nextDateInString = format(this.nextDay, 'yyyy-MM-dd');

    if (
      course.courseDate == currentDateInString ||
      course.courseDate == nextDateInString
    ) {
      course.isCourseAvailable = true;
    }
  }

  eventClicked(course: Course): void {
    if (course.isCourseAvailable) {
      this.bookingService._courseName = course.courseName;
      this.bookingService._courseDate = course.courseDate;
      this.bookingService._tutor = course.tutorName;
      this.bookingService._numberOfPoints = course.numberOfPoints;
      this.bookingService._courseId = course.courseId;
      this.router.navigate([`../book/preview`], {
        relativeTo: this.route,
      });
    }
  }
}

interface GetResponse {
  courseDetails: {
    content: Course[];
    totalElements: number;
  };
}
