import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  model: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  username: string;
  userEmail: string;
  password: string;

  submitted = false;
  signUpForm: FormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.signUpForm = this.formBuilder.group({
      username: ['', Validators.required],
      userEmail: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  get f() {
    return this.signUpForm.controls;
  }

  handleSignUp() {
    this.submitted = true;
    if (this.signUpForm.invalid) {
      return;
    }
    this.authenticationService
      .signUp(this.model.username, this.model.userEmail, this.model.password)
      .subscribe((response) => {
        alert(response.message);
      });
  }

  onReset() {
    this.submitted = false;
    this.signUpForm.reset();
    this.router.navigateByUrl('#');
  }
}
