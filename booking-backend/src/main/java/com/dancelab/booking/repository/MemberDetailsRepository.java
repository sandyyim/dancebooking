package com.dancelab.booking.repository;

import com.dancelab.booking.entity.MemberDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberDetailsRepository extends JpaRepository<MemberDetails, Long> {

    MemberDetails findByMemberId(Long memberId );

    MemberDetails findByMemberName(String memberName );

    @Query(value = "select member_id from member_details m where m.member_name =?1 ", nativeQuery = true)
    Long findMemberIdByMemberName(String memberName );

    MemberDetails findByMemberEmail(String memberEmail );



}
