package com.dancelab.booking.repository;

import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.entity.CourseDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseDetailsRepository extends JpaRepository<CourseDetails, Long>, JpaSpecificationExecutor<CourseDetails> {

    List<CourseDetails> findByCourseDate(String courseDate);

    List<CourseDetails> findByCourseDateBetween(String courseDateFrom, String courseDateTo);

    CourseDetails findByCourseId(Long courseId);

    Page<CourseDetails> findAll(Pageable pageable);

    @Query("select h from CourseDetails c,BookingHistory h where c.courseId = h.courseId and h.memberName = ?1")
    List<BookingHistory> findByMemberName(String memberName);


}
