package com.dancelab.booking.repository;

import com.dancelab.booking.entity.BookingHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingHistoryRepository extends JpaRepository<BookingHistory, Long> {

    BookingHistory findByCourseIdAndMemberId(Long courseId, Long memberId);

    BookingHistory findByCourseIdAndMemberName(Long courseId, String memberName);

    Page<BookingHistory> findAllByMemberId(Long memberId, Pageable pageable);

    Page<BookingHistory> findAllByMemberName(String memberName, Pageable pageable);

    BookingHistory findByBookingIdAndMemberId(Long bookingId, Long memberId);

    BookingHistory findByBookingIdAndMemberNameAndCancelledFalse(Long bookingId, String memberName);

}
