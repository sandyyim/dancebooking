package com.dancelab.booking.repository;

import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.entity.PackageDetails;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PackageDetailsRepository extends JpaRepository<PackageDetails, Long> {

    Page<PackageDetails> findAllByMemberName(String memberName, Pageable pageable);

    List<PackageDetails> findAllByMemberNameAndExpiredFalse(String memberName);

    PackageDetails findFirstByMemberNameAndExpiredFalse(String memberName);

}
