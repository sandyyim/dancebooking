package com.dancelab.booking;

import org.springframework.context.ApplicationEvent;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;

public class PaginatedResultsRetrievedEvent extends ApplicationEvent {

    private UriComponentsBuilder uriBuilder;
    private HttpServletResponse response;
    private Class clazz;
    private int page;
    private int totalPages;
    private int pageSize;

    public PaginatedResultsRetrievedEvent(Object source, UriComponentsBuilder uriBuilder, HttpServletResponse response, Class clazz, int page, int totalPages, int pageSize) {
        super(source);
        this.uriBuilder = uriBuilder;
        this.response = response;
        this.clazz = clazz;
        this.page = page;
        this.totalPages = totalPages;
        this.pageSize = pageSize;
    }

    public UriComponentsBuilder getUriBuilder() {
        return uriBuilder;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public Class getClazz() {
        return clazz;
    }

    public int getPage() {
        return page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getPageSize() {
        return pageSize;
    }
}
