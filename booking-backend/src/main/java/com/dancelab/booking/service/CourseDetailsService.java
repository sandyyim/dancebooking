package com.dancelab.booking.service;

import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.entity.CourseDetails;
import com.dancelab.booking.repository.CourseDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Service
public class CourseDetailsService {

    @Autowired
    private CourseDetailsRepository courseDetailsRepository;

    public List<CourseDetails> listCourseDetails(String courseDate){
        return courseDetailsRepository.findByCourseDate(courseDate);
    }

    public List<CourseDetails> listCourseDetailsWithSpecificPeriod(String courseDateFrom, String courseDateTo){
        return courseDetailsRepository.findByCourseDateBetween(courseDateFrom,courseDateTo);
    }

    public CourseDetails findCourseByCourseId(long courseId){
        return courseDetailsRepository.findByCourseId(courseId);
    }

    public CourseDetails saveCourseDetails(CourseDetails courseDetails){
        return courseDetailsRepository.save(courseDetails);
    }

    public Page<CourseDetails> listAllCourseDetails(Pageable pageable){
        return courseDetailsRepository.findAll(pageable);
    }

    public Page<CourseDetails> getAllBySpecification(final Specification<CourseDetails> specification, final Pageable pageable) {
        return courseDetailsRepository.findAll(specification, pageable);
    }

    public List<BookingHistory> findBookingRecordByMemberName(String memberName) {
        return courseDetailsRepository.findByMemberName(memberName);
    }
}
