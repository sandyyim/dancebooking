package com.dancelab.booking.service;

import com.dancelab.booking.common.DateTimeUtil;
import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.entity.MemberDetails;
import com.dancelab.booking.entity.PackageDetails;
import com.dancelab.booking.repository.BookingHistoryRepository;
import com.dancelab.booking.repository.MemberDetailsRepository;
import com.dancelab.booking.repository.PackageDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PackageDetailsService {

    @Autowired
    private PackageDetailsRepository packageDetailsRepository;

    @Autowired
    private MemberDetailsRepository memberDetailsRepository;

    public Page<PackageDetails> findAllPackageDetailsByMemberName(String memberName, Pageable pageable){
        return packageDetailsRepository.findAllByMemberName(memberName,pageable);
    }

    public PackageDetails savePackageDetails(PackageDetails packageDetails){
        return packageDetailsRepository.save(packageDetails);
    }

    public PackageDetails findMemberAvailablePoints(String memberName){
        return packageDetailsRepository.findFirstByMemberNameAndExpiredFalse(memberName);
    }

    @Scheduled(cron = "0 0 9 ? * * *")
    public void checkAndUpdateAvailablePoints(){
        List<MemberDetails> memberDetailsList = memberDetailsRepository.findAll();
        for(MemberDetails memberDetails: memberDetailsList){
            List<PackageDetails> packageDetailsList = packageDetailsRepository.findAllByMemberNameAndExpiredFalse(memberDetails.getMemberName());
            for(PackageDetails packageDetails: packageDetailsList){
                if(DateTimeUtil.compareDate1BeforeToday(packageDetails.getPackageToDate())){
                    packageDetails.setExpired(Boolean.TRUE);
                    packageDetails.setPackageBalance(0);
                    packageDetailsRepository.save(packageDetails);
                }
            }

        }
    }
}
