package com.dancelab.booking.service;

import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.repository.BookingHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class BookingHistoryService {

    @Autowired
    private BookingHistoryRepository bookingHistoryRepository;

    public BookingHistory findBookingHistoryByCourseIdAndMemberId(Long courseId, Long memberId){
        return bookingHistoryRepository.findByCourseIdAndMemberId(courseId,memberId);
    }

    public BookingHistory findBookingHistoryByCourseIdAndMemberName(Long courseId, String memberName){
        return bookingHistoryRepository.findByCourseIdAndMemberName(courseId,memberName);
    }

    public BookingHistory saveBookingHistory(BookingHistory bookingHistory){
        return bookingHistoryRepository.save(bookingHistory);
    }

    public Page<BookingHistory> findAllBookingHistoriesByMemberName(String memberName, Pageable pageable){
        return bookingHistoryRepository.findAllByMemberName(memberName,pageable);
    }

    public BookingHistory findBookingHistoryByBookingIdAndMemberId(Long bookingId, Long memberId){
        return bookingHistoryRepository.findByBookingIdAndMemberId(bookingId,memberId);
    }

    public BookingHistory findBookingHistoryByBookingIdAndMemberNameAndNotCancelled(Long bookingId, String memberName){
        return bookingHistoryRepository.findByBookingIdAndMemberNameAndCancelledFalse(bookingId,memberName);
    }

//    public void addLinkHeaderOnPagedResourceRetrieval(
//            UriComponentsBuilder uriBuilder, HttpServletResponse response,
//            Class clazz, int page, int totalPages, int size ) {
//
//        String resourceName = clazz.getSimpleName().toString().toLowerCase();
//        uriBuilder.path("/member/booking-histories/" + resourceName);
//    }
}
