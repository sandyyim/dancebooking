package com.dancelab.booking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="package_details")
public class PackageDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="package_id")
    private Long packageId;

    @Column(name="package_type")
    private String packageType;

    @Column(name="package_from_date")
    private String packageFromDate;

    @Column(name="package_to_date")
    private String packageToDate;

    @Column(name="package_balance")
    private Integer packageBalance;

    @Column(name="member_name")
    private String memberName;

    @Column(name="expired")
    private Boolean expired;

    public PackageDetails() {
    }

    public PackageDetails(Long packageId, String packageType, String packageFromDate, String packageToDate, Integer packageBalance, String memberName, Boolean expired) {
        this.packageId = packageId;
        this.packageType = packageType;
        this.packageFromDate = packageFromDate;
        this.packageToDate = packageToDate;
        this.packageBalance = packageBalance;
        this.memberName = memberName;
        this.expired = expired;
    }

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public void setPackageBalance(Integer packageBalance) {
        this.packageBalance = packageBalance;
    }

    public String getPackageFromDate() {
        return packageFromDate;
    }

    public void setPackageFromDate(String packageFromDate) {
        this.packageFromDate = packageFromDate;
    }

    public String getPackageToDate() {
        return packageToDate;
    }

    public void setPackageToDate(String packageToDate) {
        this.packageToDate = packageToDate;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Integer getPackageBalance() {
        return packageBalance;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }
}
