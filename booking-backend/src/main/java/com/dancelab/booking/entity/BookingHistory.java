package com.dancelab.booking.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="booking_history")
public class BookingHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="booking_id")
    private Long bookingId;

    @Column(name="course_id")
    private Long courseId;

    @Column(name="course_name")
    private String courseName;

    @Column(name="member_name")
    private String memberName;

    @Column(name="course_date")
    private String courseDate;

    @Column(name="course_start_time")
    private String courseStartTime;

    @Column(name="number_of_points")
    private Integer numberOfPoints;

    @Column(name="cancelled")
    private Boolean cancelled;

    @Column(name="member_id")
    private Long memberId;

//    @ManyToOne
//    @JoinColumn(name="member_id")
//    private MemberDetails memberDetails;

    public BookingHistory() {
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getCourseDate() {
        return courseDate;
    }

    public void setCourseDate(String courseDate) {
        this.courseDate = courseDate;
    }

    public String getCourseStartTime() {
        return courseStartTime;
    }

    public void setCourseStartTime(String courseStartTime) {
        this.courseStartTime = courseStartTime;
    }

    public Integer getNumberOfPoints() {
        return numberOfPoints;
    }

    public void setNumberOfPoints(Integer numberOfPoints) {
        this.numberOfPoints = numberOfPoints;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

//    public MemberDetails getMemberDetails() {
//        return memberDetails;
//    }
//
//    public void setMemberDetails(MemberDetails memberDetails) {
//        this.memberDetails = memberDetails;
//    }

    @Override
    public String toString() {
        return "BookingHistory{" +
                "bookingId=" + bookingId +
                ", courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", memberName='" + memberName + '\'' +
                ", courseDate='" + courseDate + '\'' +
                ", courseStartTime='" + courseStartTime + '\'' +
                ", numberOfPoints=" + numberOfPoints +
                ", cancelled=" + cancelled +
                ", fkMemberId=" + memberId +
//                ", memberDetails=" + memberDetails +
                '}';
    }
}
