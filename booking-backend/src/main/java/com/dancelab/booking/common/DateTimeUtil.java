package com.dancelab.booking.common;

import java.time.LocalDate;

public class DateTimeUtil {

    public static boolean compareDate1BeforeDate2(String dateStr1, String dateStr2){
        LocalDate date1 = LocalDate.parse(dateStr1);
        LocalDate date2 = LocalDate.parse(dateStr2);

        return date1.isBefore(date2);

    }

    public static boolean compareDate1BeforeToday(String dateStr1){
        LocalDate date1 = LocalDate.parse(dateStr1);

        return date1.isBefore(LocalDate.now());

    }
}
