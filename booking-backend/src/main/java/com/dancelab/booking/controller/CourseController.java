package com.dancelab.booking.controller;

import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.entity.CourseDetails;
import com.dancelab.booking.entity.MemberDetails;
import com.dancelab.booking.entity.PackageDetails;
import com.dancelab.booking.model.CourseBookingResponseModel;
import com.dancelab.booking.service.BookingHistoryService;
import com.dancelab.booking.service.CourseDetailsService;
import com.dancelab.booking.service.CourseSearchService;
import com.dancelab.booking.service.MemberDetailsService;
import com.dancelab.booking.service.PackageDetailsService;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.LessThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.awt.print.Book;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin("http://localhost:4200")
@RestController
public class CourseController {

    @Autowired
    private CourseDetailsService courseDetailsService;

    @Autowired
    private MemberDetailsService memberDetailsService;

    @Autowired
    private BookingHistoryService bookingHistoryService;

    @Autowired
    private CourseSearchService courseSearchService;

    @Autowired
    private PackageDetailsService packageDetailsService;

    @PostMapping("/course/browse")
    public List<CourseDetails> listCourseDetails(@RequestParam(name = "from") String courseDateFrom, @RequestParam(name = "to") String courseDateTo) {
        List<CourseDetails> courseDetailsList = courseDetailsService.listCourseDetailsWithSpecificPeriod(courseDateFrom, courseDateTo);
        return courseDetailsList;
    }

    @PostMapping("/course/book")
    public CourseBookingResponseModel bookCourse(@RequestParam(name = "member_name") String memberName, @RequestParam(name = "course_id") long courseId) {
        PackageDetails packageDetails = packageDetailsService.findMemberAvailablePoints(memberName);
//        MemberDetails memberDetails = memberDetailsService.loadMemberByMemberName(memberName);
        int memberAvailablePoints = packageDetails.getPackageBalance();

        CourseDetails courseDetails = courseDetailsService.findCourseByCourseId(courseId);
        int courseCapacity = courseDetails.getCapacity();
        int coursePoints = courseDetails.getNumberOfPoints();

        BookingHistory existingBookingHistory = bookingHistoryService.findBookingHistoryByCourseIdAndMemberName(courseId,memberName);

        CourseBookingResponseModel courseBookingResponseModel = new CourseBookingResponseModel();
        if (memberAvailablePoints < coursePoints) {
            courseBookingResponseModel.setStatus("Failed");
            courseBookingResponseModel.setReturnMessage("Not Enough Points");
        } else if (courseCapacity == 0) {
            courseBookingResponseModel.setStatus("Failed");
            courseBookingResponseModel.setReturnMessage("Booking is full");
        } else if (existingBookingHistory != null && existingBookingHistory.getCancelled() == Boolean.FALSE) {
            courseBookingResponseModel.setStatus("Failed");
            courseBookingResponseModel.setReturnMessage("You have booked the course");
        } else {
            BookingHistory bookingHistory = new BookingHistory();
            courseDetails.setCapacity(courseCapacity - 1);
            packageDetails.setPackageBalance(memberAvailablePoints - coursePoints);
            bookingHistory.setCourseDate(courseDetails.getCourseDate());
            bookingHistory.setCourseId(courseId);
            bookingHistory.setCourseName(courseDetails.getCourseName());
            bookingHistory.setMemberName(memberName);
            bookingHistory.setCourseStartTime(courseDetails.getCourseStartTime());
            bookingHistory.setNumberOfPoints(coursePoints);
            bookingHistory.setCancelled(Boolean.FALSE);
//            bookingHistory.setMemberId(memberDetails.getMemberId());


            courseDetailsService.saveCourseDetails(courseDetails);
            packageDetailsService.savePackageDetails(packageDetails);
//            memberDetailsService.saveMemberDetails(memberDetails);
            bookingHistoryService.saveBookingHistory(bookingHistory);
            courseBookingResponseModel.setStatus("Success");
        }


        return courseBookingResponseModel;
    }

    @PostMapping("/course/search/book")
    public Map<Long, Boolean> checkMemberBookCourseOrNot(@RequestParam(name = "member_name") String memberName) {
        List<BookingHistory> bookingHistoryList = courseDetailsService.findBookingRecordByMemberName(memberName);

        Map<Long, Boolean> response = new HashMap<>();
        for (BookingHistory bookingHistory:bookingHistoryList){
            response.put(bookingHistory.getCourseId(),bookingHistory.getCancelled());
        }

        return response;
    }

    @PostMapping("/course/search")
    public ResponseEntity<Map<String, Object>> search(@And({
            @Spec(path = "courseName", spec = LikeIgnoreCase.class),
            @Spec(path = "tutorName", spec = LikeIgnoreCase.class),
            @Spec(path = "courseDate", params = "fromDate", spec = GreaterThanOrEqual.class),
            @Spec(path = "courseDate", params = "toDate", spec = LessThanOrEqual.class)
    }) Specification<CourseDetails> specification,
                                      @RequestParam(defaultValue = "0") int page,
                                      @RequestParam(defaultValue = "10") int size) {

        Pageable sortedByCourseDateDesc =
                PageRequest.of(page, size, Sort.by("courseDate").descending());

        Page<CourseDetails> courseDetails = courseDetailsService.getAllBySpecification(specification, sortedByCourseDateDesc);

        Map<String, Object> response = new HashMap<>();
        response.put("courseDetails", courseDetails);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/test/update")
    public String testUpdate(){
        try{
            packageDetailsService.checkAndUpdateAvailablePoints();
            return "Success";
        }catch (Exception e){
            return "Failed";
        }
    }

}
