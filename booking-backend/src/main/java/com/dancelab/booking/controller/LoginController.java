package com.dancelab.booking.controller;

import com.dancelab.booking.common.JwtTokenUtil;
import com.dancelab.booking.entity.MemberDetails;
import com.dancelab.booking.model.*;
import com.dancelab.booking.service.MemberDetailsService;
import com.dancelab.booking.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Base64;
import java.util.Locale;

@RestController
@CrossOrigin("http://localhost:4200")
public class LoginController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private MemberDetailsService memberDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/user")
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
                .substring("Basic".length()).trim();
        return () -> new String(Base64.getDecoder()
                .decode(authToken)).split(":")[0];
    }

    @PostMapping("/signup")
    public SignUpResponse signup(@Valid @RequestBody SignUpRequest signUpRequest) throws Exception {
        SignUpResponse signUpResponse = new SignUpResponse();
        if (memberDetailsService.findByMemberName(signUpRequest.getUsername())) {
            signUpResponse.setStatus("Failed");
            signUpResponse.setMessage("User already exists!");
            return signUpResponse;
        }

        if (memberDetailsService.findByMemberEmail(signUpRequest.getUserEmail())) {
            signUpResponse.setStatus("Failed");
            signUpResponse.setMessage("Email is already taken!");
            return signUpResponse;
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        // Create new user's account
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setMemberName(signUpRequest.getUsername());
        memberDetails.setMemberEmail(signUpRequest.getUserEmail());
        memberDetails.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

        memberDetailsService.saveMemberDetails(memberDetails);

        signUpResponse.setStatus("Success");
        signUpResponse.setMessage("Success");
        return signUpResponse;
    }

    @RequestMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(authenticationRequest.getUsername());
//        final Long memberId = memberDetailsService.loadMemberIdByMemberName(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);
//        return ResponseEntity.ok(new JwtResponse(token,memberId));
        return ResponseEntity.ok(new JwtResponse(token));

    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @PostMapping("/changepassword")
    public ChangePasswordResponseModel changePassword(@RequestBody PasswordDto password) throws Exception {
        ChangePasswordResponseModel changePasswordResponseModel = new ChangePasswordResponseModel();
        try{
            authenticate(password.getUsername(), password.getOldPassword());
            final MemberDetails memberDetail = memberDetailsService.loadMemberByMemberName(password.getUsername());
            memberDetail.setPassword(passwordEncoder.encode(password.getNewPassword()));
            memberDetailsService.saveMemberDetails(memberDetail);
            changePasswordResponseModel.setStatus("Success");
            return changePasswordResponseModel;
        }catch(Exception e){
            changePasswordResponseModel.setStatus("Failed");
            changePasswordResponseModel.setReturnMessage("Failed to update password");

            return changePasswordResponseModel;
        }

    }
}
