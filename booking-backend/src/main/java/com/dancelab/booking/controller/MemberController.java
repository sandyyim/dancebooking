package com.dancelab.booking.controller;

import com.dancelab.booking.PaginatedResultsRetrievedEventDiscoverabilityListener;
import com.dancelab.booking.entity.BookingHistory;
import com.dancelab.booking.entity.MemberDetails;
import com.dancelab.booking.entity.PackageDetails;
import com.dancelab.booking.service.BookingHistoryService;
import com.dancelab.booking.service.MemberDetailsService;
import com.dancelab.booking.service.PackageDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin("http://localhost:4200")
@RestController
public class MemberController {

    @Autowired
    private MemberDetailsService memberDetailsService;

    @Autowired
    private BookingHistoryService bookingHistoryService;

    @Autowired
    private PaginatedResultsRetrievedEventDiscoverabilityListener eventDiscoverabilityListener;

    @Autowired
    private PackageDetailsService packageDetailsService;

//    @GetMapping("/test")
//    public String test(@RequestBody TestModel testModel) {
//        return testModel.toString();
//    }

    @PostMapping("/member/create")
    public MemberDetails showMemberName(@RequestBody String username) {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setMemberName("test");
        memberDetailsService.saveMemberDetails(memberDetails);
        return memberDetails;
    }

    @PostMapping("/member/details")
    public MemberDetails showMemberDetails(@RequestParam(name = "member_name") String memberName) {
        // display member details
        MemberDetails memberDetails = memberDetailsService.loadMemberByMemberName(memberName);
        return memberDetails;
    }

    @PostMapping("/member/{memberName}/booking-histories")
//    @GetMapping(params = {"page", "size"})
    public ResponseEntity<Map<String, Object>> loadBookingHistories(@RequestParam(defaultValue = "0") int page,
                                                                    @RequestParam(defaultValue = "10") int size, @PathVariable("memberName") String memberName) {
        Pageable sortedByCourseDateDesc =
                PageRequest.of(page, size, Sort.by("courseDate").descending());

        Page<BookingHistory> bookingHistories = bookingHistoryService.findAllBookingHistoriesByMemberName(memberName, sortedByCourseDateDesc);

        if (bookingHistories.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("bookingHistories", bookingHistories);
//        response.put("currentPage", bookingHistories.getNumber());
//        response.put("totalItems", bookingHistories.getTotalElements());
//        response.put("totalPages", bookingHistories.getTotalPages());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/member/booking/cancel")
    public ResponseEntity<Object> cancelBooking(@RequestParam(name = "member_name") String memberName, @RequestParam(name="booking_id") Long bookingId) {
        // display member details
        BookingHistory bookingHistory = bookingHistoryService.findBookingHistoryByBookingIdAndMemberNameAndNotCancelled(bookingId,memberName);
        if(bookingHistory!=null ){
            bookingHistory.setCancelled(Boolean.TRUE);
            bookingHistoryService.saveBookingHistory(bookingHistory);
            return new ResponseEntity<>(bookingHistory,HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/member/package")
    public ResponseEntity<Object> viewPackages(@RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "10") int size, @RequestParam(name = "member_name") String memberName) {
        Pageable sortedByPackagePurchaseDateDesc =
                PageRequest.of(page, size, Sort.by("packageFromDate").descending());

        Page<PackageDetails> packageDetails= packageDetailsService.findAllPackageDetailsByMemberName(memberName,sortedByPackagePurchaseDateDesc);

        if (packageDetails.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        Map<String, Object> response = new HashMap<>();
        response.put("packageDetails", packageDetails);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
