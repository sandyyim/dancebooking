package com.dancelab.booking.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;

    private final String jwttoken;

    private Long memberId;

//    public JwtResponse(String jwttoken, Long memberId) {
//        this.jwttoken = jwttoken;
//        this.memberId = memberId;
//
//    }

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return this.jwttoken;
    }

}

