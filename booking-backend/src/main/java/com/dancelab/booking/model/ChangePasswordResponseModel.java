package com.dancelab.booking.model;

public class ChangePasswordResponseModel {

    private String status;
    private String returnMessage;

    public ChangePasswordResponseModel() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    @Override
    public String toString() {
        return "ChangePasswordResponseModel{" +
                "status='" + status + '\'' +
                ", returnMessage='" + returnMessage + '\'' +
                '}';
    }
}
